import { SCREEN_SIZE, BACKGROUND_COLOR } from './configuration/constants';

function threadCallback (event) {
  const { image, color } = event.data;
  let result = 0;
  const maxPixels = image.width * image.height * 4;

  for (let px = 0; px < maxPixels; px += 4) {
    const pixel = [image.data[px], image.data[px + 1], image.data[px + 2]];
    if (pixel[0] !== color.r || pixel[1] !== color.g || pixel[2] !== color.b) {
      ++result;
    }
  }
  self.postMessage(result);
}

export function initWorker(context, callback) {
  if (!window.Worker) {
    return;
  }

  const blob = new Blob(['onmessage =' + threadCallback.toString()], { type: `text/javascript` });

  const worker = new Worker(window.URL.createObjectURL(blob));
  worker.onmessage = function (event) {
    callback(event.data);
  };

  setInterval(() => {
    const image = context.getImageData(0, 0, SCREEN_SIZE.W, SCREEN_SIZE.H);
    const color = hexToRgb(BACKGROUND_COLOR);
    const data = { image, color };
    worker.postMessage(data);
  }, 1000 / 60);
}

function hexToRgb(value) {
  return { b: value & 255, g:(value >> 8) & 255, r: (value >> 16) & 255 };
}