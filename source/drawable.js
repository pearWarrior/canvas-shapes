import { WIDTH, HEIGHT } from "./configuration/constants";

export class Drawable extends PIXI.Graphics {
  constructor(position = new PIXI.Point(0, 0), width = WIDTH, height = HEIGHT) {
    super();
    this.container = { width, height };
    this.setPosition(position);
    this.onDraw();
  }

  setPosition(point) {
    this.position.set(point.x - this.container.width / 2, point.y - this.container.height / 2);
  }

  onDraw() {
    throw Error('Must be override.');
  }
}