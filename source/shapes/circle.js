import Factory from '../factory';
import { SHAPE } from '../configuration/constants';
import { Gravitable } from "../gravitable";

class Circle extends Gravitable {
  constructor({ position, removeCallback }) {
    super(position, removeCallback);
  }

  onDraw() {
    const color = Math.floor(Math.random() * 0xffffff);
    this.beginFill(color);
    const halfWidth = this.container.width / 2;
    this.drawCircle(halfWidth, halfWidth, halfWidth);
    this.endFill();
  }
}

Factory.registerShape(SHAPE.CIRCLE, (data) => new Circle(data));
