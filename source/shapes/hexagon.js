import Factory from '../factory';
import { SHAPE } from '../configuration/constants';
import { Gravitable } from "../gravitable";

class Hexagon extends Gravitable {
  constructor({ position, removeCallback }) {
    super(position, removeCallback);
  }

  onDraw() {
    const color = Math.floor(Math.random() * 0xffffff);
    this.beginFill(color);

    const { width, height } = this.container;
    this.moveTo(0, height / 4);
    this.lineTo(width / 2, 0);
    this.lineTo(width, height / 4);
    this.lineTo(width, height / 1.3);
    this.lineTo(width / 2, height);
    this.lineTo(0, height / 1.3);
    this.lineTo(0, height / 4);
    this.endFill();
  }
}

Factory.registerShape(SHAPE.HEXAGON, (data) => new Hexagon(data));
