import Factory from '../factory';
import { SHAPE } from '../configuration/constants';
import { Gravitable } from "../gravitable";

class Pentagon extends Gravitable {
  constructor({ position, removeCallback }) {
    super(position, removeCallback);
  }

  onDraw() {
    const color = Math.floor(Math.random() * 0xffffff);
    this.beginFill(color);

    const { width, height } = this.container;
    this.moveTo(0, 0);
    this.lineTo(width, 0);
    this.lineTo(width / 2, height / 2);
    this.lineTo(width, height);
    this.lineTo(0, height);
    this.lineTo(0, 0);
    this.endFill();
  }
}

Factory.registerShape(SHAPE.PENTAGON, (data) => new Pentagon(data));
