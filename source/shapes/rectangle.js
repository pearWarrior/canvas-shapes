import Factory from '../factory';
import { SHAPE } from '../configuration/constants';
import { Gravitable } from "../gravitable";

class Rectangle extends Gravitable {
  constructor({ position, removeCallback }) {
    super(position, removeCallback);
  }

  onDraw() {
    const color = Math.floor(Math.random() * 0xffffff);
    this.beginFill(color);
    this.drawRect(0, 0, this.container.width, this.container.height);
    this.endFill();
  }
}

Factory.registerShape(SHAPE.RECTANGLE, (data) => new Rectangle(data));
