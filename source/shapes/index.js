import "./rectangle";
import "./triangle";
import "./circle";
import "./ellipse";
import "./hexagon";
import "./pentagon";
