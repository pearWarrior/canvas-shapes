import Factory from '../factory';
import { SHAPE } from '../configuration/constants';
import { Gravitable } from "../gravitable";

class Ellipse extends Gravitable {
  constructor({ position, removeCallback }) {
    super(position, removeCallback);
  }

  onDraw() {
    const color = Math.floor(Math.random() * 0xffffff);
    this.beginFill(color);
    const halfWidth = this.container.width / 2;
    this.drawEllipse(halfWidth, halfWidth, halfWidth, halfWidth / 2);
    this.endFill();
  }
}

Factory.registerShape(SHAPE.ELLIPSE, (data) => new Ellipse(data));
