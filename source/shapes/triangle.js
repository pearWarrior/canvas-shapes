import Factory from '../factory';
import { SHAPE } from '../configuration/constants';
import { Gravitable } from "../gravitable";

class Triangle extends Gravitable {
  constructor({ position, removeCallback }) {
    super(position, removeCallback);
  }

  onDraw() {
    const color = Math.floor(Math.random() * 0xffffff);
    this.beginFill(color);

    const { width, height } = this.container;
    this.moveTo(width / 2, 0);
    this.lineTo(width, height);
    this.lineTo(0, height);
    this.lineTo(this.container.width / 2, 0);
    this.endFill();
  }
}

Factory.registerShape(SHAPE.TRIANGLE, (data) => new Triangle(data));
