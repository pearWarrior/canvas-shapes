import { Drawable } from './drawable';

export class Gravitable extends Drawable {
  constructor(position, removeCallback) {
    super(position);
    this.updater = (dt) => this.update(dt);
    PIXI.ticker.shared.add(this.updater);
    this.removeCallback = removeCallback;
    this.changeGravity(0);
  }

  update(dt) {
    if (this.isBehindScreen()) {
      PIXI.ticker.shared.remove(this.updater);
      this.onMoveEnd();
    }
    this.position.y += this.gravity * dt;
  }

  isBehindScreen() {
    return this.position.y > innerHeight - this.container.height;
  }

  onMoveEnd() {
    this.removeCallback(this);
  }

  changeGravity(value) {
    this.gravity = value;
  }
}