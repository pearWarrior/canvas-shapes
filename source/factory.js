const factory = {};

class Factory {
  registerShape(type, callback) {
    factory[ type ] = callback;
  }

  createShape(type, data) {
    return factory[ type ](data);
  }

  static getInstance() {
    let instance = null;
    return instance === null && instance || (instance = new Factory());
  }
}

export default Factory.getInstance();
