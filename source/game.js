import { SHAPE, WIDTH, HEIGHT, SCREEN_SIZE, BACKGROUND_COLOR } from './configuration/constants';
import Factory from './factory';
import { initWorker } from './canvasWorker';
import "./shapes/index";

let stage = null;

let shapeTotal = document.querySelector('.head.shape-counter span');
let shapeOccupied = document.querySelector('.head.shape-occupied span');
let shapePerSecond = document.querySelector('.footer .shape-counter span');
let gravity = document.querySelector('.gravity span');

const shapes = [];

export default class Game {
  constructor() {
    this.renderer = new PIXI.CanvasRenderer(SCREEN_SIZE.W, SCREEN_SIZE.H);
    document.getElementsByClassName('game-container')[ 0 ].appendChild(this.renderer.view);

    stage = new PIXI.Graphics();
    stage.interactive = true;
    stage.on('pointerdown', (event) => this.onClick(event));


    /** init background */
    stage.beginFill(BACKGROUND_COLOR, 1);
    stage.drawRect(0, 0, SCREEN_SIZE.W, SCREEN_SIZE.H);
    stage.endFill();

    PIXI.ticker.shared.start();

    this.initEvents();
    this.initScheduler();
    this.gameLoop();
    initWorker(this.renderer.context, (pixels) => shapeOccupied.innerHTML = pixels);
  }

  initEvents() {
    let handlers = document.querySelector('.shape-counter.handlers');
    handlers.querySelector('.minus').onclick = () => this.changeShapePerSecond(-1);
    handlers.querySelector('.plus').onclick = () => this.changeShapePerSecond(1);

    handlers = document.querySelector('.gravity.handlers');
    handlers.querySelector('.minus').onclick = () => this.changeGravity(-1);
    handlers.querySelector('.plus').onclick = () => this.changeGravity(1);
  }

  initScheduler() {
    const generateShape = () => {
      const x = Math.floor(Math.random() * window.innerWidth - WIDTH);
      const y = Math.floor(Math.random() * window.innerHeight - HEIGHT);
      const type = Math.floor(Math.random() * SHAPE.MAX);
      const dataToShape = { position: { x, y }, removeCallback: (shape) => this.onRemoveShape(shape) };
      const shape = Factory.createShape(type, dataToShape);
      this.onAddShape(shape);
    };

    setInterval(() => {
      let i = Number(shapePerSecond.innerHTML);
      for (; i > 0; i--) {
        generateShape();
      }
    }, 1000);
  }

  gameLoop() {
    this.renderer.render(stage);
    requestAnimationFrame(this.gameLoop.bind(this));
  }

  onClick(event) {
    const mousePosition = event.data.getLocalPosition(stage);
    const dataToShape = { position: mousePosition, removeCallback: (shape) => this.onRemoveShape(shape) };
    const type = Math.floor(Math.random() * SHAPE.MAX);
    const shape = Factory.createShape(type, dataToShape);
    this.onAddShape(shape);
  }

  onAddShape(shape) {
    stage.addChild(shape);
    this.changeShapeCount(1);
    shape.changeGravity(Number(gravity.innerHTML));
    shapes.push(shape);
  }

  onRemoveShape(shape) {
    stage.removeChild(shape);
    this.changeShapeCount(-1);
    shapes.splice(shapes.indexOf(shape), 1);
  }

  changeShapeCount(value) {
    shapeTotal.innerHTML = Number(shapeTotal.innerHTML) + value;
  }

  changeShapePerSecond(value) {
    shapePerSecond.innerHTML = Number(shapePerSecond.innerHTML) + value;
  }

  changeGravity(value) {
    gravity.innerHTML = Number(gravity.innerHTML) + value;
    shapes.forEach(shape => shape.changeGravity(gravity.innerHTML));
  }
}
