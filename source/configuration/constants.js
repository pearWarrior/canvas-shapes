export const WIDTH = 120;
export const HEIGHT = 120;

let i = 0;
export const SHAPE = Object.freeze({
  RECTANGLE: i++,
  TRIANGLE: i++,
  CIRCLE: i++,
  ELLIPSE: i++,
  PENTAGON: i++,
  HEXAGON: i++,
  MAX: i++
});

export const SCREEN_SIZE = {
  W: window.innerWidth * 0.8,
  H: window.innerHeight * 0.8
};

export const BACKGROUND_COLOR = 0x008299;