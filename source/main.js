import Game from './game';

class Application {
  constructor() {
    this.game = new Game();
  }
}

new Application();