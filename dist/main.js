(function () {
'use strict';

var WIDTH = 120;
var HEIGHT = 120;



var i = 0;
var SHAPE = Object.freeze({
  RECTANGLE: i++,
  TRIANGLE: i++,
  CIRCLE: i++,
  ELLIPSE: i++,
  PENTAGON: i++,
  HEXAGON: i++,
  MAX: i++
});

var SCREEN_SIZE = {
  W: window.innerWidth * 0.8,
  H: window.innerHeight * 0.8
};

var BACKGROUND_COLOR = 0x008299;

var asyncGenerator = function () {
  function AwaitValue(value) {
    this.value = value;
  }

  function AsyncGenerator(gen) {
    var front, back;

    function send(key, arg) {
      return new Promise(function (resolve, reject) {
        var request = {
          key: key,
          arg: arg,
          resolve: resolve,
          reject: reject,
          next: null
        };

        if (back) {
          back = back.next = request;
        } else {
          front = back = request;
          resume(key, arg);
        }
      });
    }

    function resume(key, arg) {
      try {
        var result = gen[key](arg);
        var value = result.value;

        if (value instanceof AwaitValue) {
          Promise.resolve(value.value).then(function (arg) {
            resume("next", arg);
          }, function (arg) {
            resume("throw", arg);
          });
        } else {
          settle(result.done ? "return" : "normal", result.value);
        }
      } catch (err) {
        settle("throw", err);
      }
    }

    function settle(type, value) {
      switch (type) {
        case "return":
          front.resolve({
            value: value,
            done: true
          });
          break;

        case "throw":
          front.reject(value);
          break;

        default:
          front.resolve({
            value: value,
            done: false
          });
          break;
      }

      front = front.next;

      if (front) {
        resume(front.key, front.arg);
      } else {
        back = null;
      }
    }

    this._invoke = send;

    if (typeof gen.return !== "function") {
      this.return = undefined;
    }
  }

  if (typeof Symbol === "function" && Symbol.asyncIterator) {
    AsyncGenerator.prototype[Symbol.asyncIterator] = function () {
      return this;
    };
  }

  AsyncGenerator.prototype.next = function (arg) {
    return this._invoke("next", arg);
  };

  AsyncGenerator.prototype.throw = function (arg) {
    return this._invoke("throw", arg);
  };

  AsyncGenerator.prototype.return = function (arg) {
    return this._invoke("return", arg);
  };

  return {
    wrap: function (fn) {
      return function () {
        return new AsyncGenerator(fn.apply(this, arguments));
      };
    },
    await: function (value) {
      return new AwaitValue(value);
    }
  };
}();





var classCallCheck = function (instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
};

var createClass = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);
    if (staticProps) defineProperties(Constructor, staticProps);
    return Constructor;
  };
}();









var inherits = function (subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      enumerable: false,
      writable: true,
      configurable: true
    }
  });
  if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
};











var possibleConstructorReturn = function (self, call) {
  if (!self) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return call && (typeof call === "object" || typeof call === "function") ? call : self;
};

var factory = {};

var Factory = function () {
  function Factory() {
    classCallCheck(this, Factory);
  }

  createClass(Factory, [{
    key: "registerShape",
    value: function registerShape(type, callback) {
      factory[type] = callback;
    }
  }, {
    key: "createShape",
    value: function createShape(type, data) {
      return factory[type](data);
    }
  }], [{
    key: "getInstance",
    value: function getInstance() {
      var instance = null;
      return instance === null && instance || (instance = new Factory());
    }
  }]);
  return Factory;
}();

var Factory$1 = Factory.getInstance();

function threadCallback(event) {
  var _event$data = event.data,
      image = _event$data.image,
      color = _event$data.color;

  var result = 0;
  var maxPixels = image.width * image.height * 4;

  for (var px = 0; px < maxPixels; px += 4) {
    var pixel = [image.data[px], image.data[px + 1], image.data[px + 2]];
    if (pixel[0] !== color.r || pixel[1] !== color.g || pixel[2] !== color.b) {
      ++result;
    }
  }
  self.postMessage(result);
}

function initWorker(context, callback) {
  if (!window.Worker) {
    return;
  }

  var blob = new Blob(['onmessage =' + threadCallback.toString()], { type: 'text/javascript' });

  var worker = new Worker(window.URL.createObjectURL(blob));
  worker.onmessage = function (event) {
    callback(event.data);
  };

  setInterval(function () {
    var image = context.getImageData(0, 0, SCREEN_SIZE.W, SCREEN_SIZE.H);
    var color = hexToRgb(BACKGROUND_COLOR);
    var data = { image: image, color: color };
    worker.postMessage(data);
  }, 1000 / 60);
}

function hexToRgb(value) {
  return { b: value & 255, g: value >> 8 & 255, r: value >> 16 & 255 };
}

var Drawable = function (_PIXI$Graphics) {
  inherits(Drawable, _PIXI$Graphics);

  function Drawable() {
    var position = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : new PIXI.Point(0, 0);
    var width = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : WIDTH;
    var height = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : HEIGHT;
    classCallCheck(this, Drawable);

    var _this = possibleConstructorReturn(this, (Drawable.__proto__ || Object.getPrototypeOf(Drawable)).call(this));

    _this.container = { width: width, height: height };
    _this.setPosition(position);
    // this.anchor.set(0.5);
    // this.lineStyle(2, 0x000000);
    // this.beginFill(0xFF700B, 1);
    // this.drawRect(50, 250, 120, 120);

    _this.onDraw();
    return _this;
  }

  createClass(Drawable, [{
    key: "setPosition",
    value: function setPosition(point) {
      this.position.set(point.x - this.container.width / 2, point.y - this.container.height / 2);
      // this.position.set(point.x, point.y);
    }
  }, {
    key: "onDraw",
    value: function onDraw() {
      throw Error('Must be override.');
      // this.clear();
      // this.lineStyle(2, 0x000000);
      // this.beginFill(0xFF700B, 1);
      // this.drawRect(0, 0, 120, 120);
      // this.endFill();
    }
  }]);
  return Drawable;
}(PIXI.Graphics);

var Gravitable = function (_Drawable) {
  inherits(Gravitable, _Drawable);

  function Gravitable(position, removeCallback) {
    classCallCheck(this, Gravitable);

    var _this = possibleConstructorReturn(this, (Gravitable.__proto__ || Object.getPrototypeOf(Gravitable)).call(this, position));

    _this.updater = function (dt) {
      return _this.update(dt);
    };
    PIXI.ticker.shared.add(_this.updater);
    _this.removeCallback = removeCallback;
    _this.changeGravity(0);
    return _this;
  }

  createClass(Gravitable, [{
    key: 'update',
    value: function update(dt) {
      if (this.isBehindScreen()) {
        PIXI.ticker.shared.remove(this.updater);
        this.onMoveEnd();
      }
      this.position.y += this.gravity * dt;
    }
  }, {
    key: 'isBehindScreen',
    value: function isBehindScreen() {
      return this.position.y > innerHeight - this.container.height;
    }
  }, {
    key: 'onMoveEnd',
    value: function onMoveEnd() {
      this.removeCallback(this);
    }
  }, {
    key: 'changeGravity',
    value: function changeGravity(value) {
      this.gravity = value;
    }
  }]);
  return Gravitable;
}(Drawable);

var Rectangle = function (_Gravitable) {
  inherits(Rectangle, _Gravitable);

  function Rectangle(_ref) {
    var position = _ref.position,
        removeCallback = _ref.removeCallback;
    classCallCheck(this, Rectangle);
    return possibleConstructorReturn(this, (Rectangle.__proto__ || Object.getPrototypeOf(Rectangle)).call(this, position, removeCallback));
  }

  createClass(Rectangle, [{
    key: 'onDraw',
    value: function onDraw() {
      var color = Math.floor(Math.random() * 0xffffff);
      this.beginFill(color);
      this.drawRect(0, 0, this.container.width, this.container.height);
      this.endFill();
    }
  }]);
  return Rectangle;
}(Gravitable);

Factory$1.registerShape(SHAPE.RECTANGLE, function (data) {
  return new Rectangle(data);
});

var Triangle = function (_Gravitable) {
  inherits(Triangle, _Gravitable);

  function Triangle(_ref) {
    var position = _ref.position,
        removeCallback = _ref.removeCallback;
    classCallCheck(this, Triangle);
    return possibleConstructorReturn(this, (Triangle.__proto__ || Object.getPrototypeOf(Triangle)).call(this, position, removeCallback));
  }

  createClass(Triangle, [{
    key: 'onDraw',
    value: function onDraw() {
      var color = Math.floor(Math.random() * 0xffffff);
      this.beginFill(color);

      var _container = this.container,
          width = _container.width,
          height = _container.height;

      this.moveTo(width / 2, 0);
      this.lineTo(width, height);
      this.lineTo(0, height);
      this.lineTo(this.container.width / 2, 0);
      this.endFill();
    }
  }]);
  return Triangle;
}(Gravitable);

Factory$1.registerShape(SHAPE.TRIANGLE, function (data) {
  return new Triangle(data);
});

var Circle = function (_Gravitable) {
  inherits(Circle, _Gravitable);

  function Circle(_ref) {
    var position = _ref.position,
        removeCallback = _ref.removeCallback;
    classCallCheck(this, Circle);
    return possibleConstructorReturn(this, (Circle.__proto__ || Object.getPrototypeOf(Circle)).call(this, position, removeCallback));
  }

  createClass(Circle, [{
    key: 'onDraw',
    value: function onDraw() {
      var color = Math.floor(Math.random() * 0xffffff);
      this.beginFill(color);
      var halfWidth = this.container.width / 2;
      this.drawCircle(halfWidth, halfWidth, halfWidth);
      this.endFill();
    }
  }]);
  return Circle;
}(Gravitable);

Factory$1.registerShape(SHAPE.CIRCLE, function (data) {
  return new Circle(data);
});

var Ellipse = function (_Gravitable) {
  inherits(Ellipse, _Gravitable);

  function Ellipse(_ref) {
    var position = _ref.position,
        removeCallback = _ref.removeCallback;
    classCallCheck(this, Ellipse);
    return possibleConstructorReturn(this, (Ellipse.__proto__ || Object.getPrototypeOf(Ellipse)).call(this, position, removeCallback));
  }

  createClass(Ellipse, [{
    key: 'onDraw',
    value: function onDraw() {
      var color = Math.floor(Math.random() * 0xffffff);
      this.beginFill(color);
      var halfWidth = this.container.width / 2;
      this.drawEllipse(halfWidth, halfWidth, halfWidth, halfWidth / 2);
      this.endFill();
    }
  }]);
  return Ellipse;
}(Gravitable);

Factory$1.registerShape(SHAPE.ELLIPSE, function (data) {
  return new Ellipse(data);
});

var Hexagon = function (_Gravitable) {
  inherits(Hexagon, _Gravitable);

  function Hexagon(_ref) {
    var position = _ref.position,
        removeCallback = _ref.removeCallback;
    classCallCheck(this, Hexagon);
    return possibleConstructorReturn(this, (Hexagon.__proto__ || Object.getPrototypeOf(Hexagon)).call(this, position, removeCallback));
  }

  createClass(Hexagon, [{
    key: 'onDraw',
    value: function onDraw() {
      var color = Math.floor(Math.random() * 0xffffff);
      this.beginFill(color);

      var _container = this.container,
          width = _container.width,
          height = _container.height;

      this.moveTo(0, height / 4);
      this.lineTo(width / 2, 0);
      this.lineTo(width, height / 4);
      this.lineTo(width, height / 1.3);
      this.lineTo(width / 2, height);
      this.lineTo(0, height / 1.3);
      this.lineTo(0, height / 4);
      this.endFill();
    }
  }]);
  return Hexagon;
}(Gravitable);

Factory$1.registerShape(SHAPE.HEXAGON, function (data) {
  return new Hexagon(data);
});

var Pentagon = function (_Gravitable) {
  inherits(Pentagon, _Gravitable);

  function Pentagon(_ref) {
    var position = _ref.position,
        removeCallback = _ref.removeCallback;
    classCallCheck(this, Pentagon);
    return possibleConstructorReturn(this, (Pentagon.__proto__ || Object.getPrototypeOf(Pentagon)).call(this, position, removeCallback));
  }

  createClass(Pentagon, [{
    key: 'onDraw',
    value: function onDraw() {
      var color = Math.floor(Math.random() * 0xffffff);
      this.beginFill(color);

      var _container = this.container,
          width = _container.width,
          height = _container.height;

      this.moveTo(0, 0);
      this.lineTo(width, 0);
      this.lineTo(width / 2, height / 2);
      this.lineTo(width, height);
      this.lineTo(0, height);
      this.lineTo(0, 0);
      this.endFill();
    }
  }]);
  return Pentagon;
}(Gravitable);

Factory$1.registerShape(SHAPE.PENTAGON, function (data) {
  return new Pentagon(data);
});

var stage = null;

var shapeTotal = document.querySelector('.head.shape-counter span');
var shapeOccupied = document.querySelector('.head.shape-occupied span');
var shapePerSecond = document.querySelector('.footer .shape-counter span');
var gravity = document.querySelector('.gravity span');

var shapes = [];

var Game = function () {
  function Game() {
    var _this = this;

    classCallCheck(this, Game);

    this.renderer = new PIXI.CanvasRenderer(SCREEN_SIZE.W, SCREEN_SIZE.H);
    document.getElementsByClassName('game-container')[0].appendChild(this.renderer.view);

    stage = new PIXI.Graphics();
    stage.interactive = true;
    stage.on('pointerdown', function (event) {
      return _this.onClick(event);
    });

    /** init background */
    stage.beginFill(BACKGROUND_COLOR, 1);
    stage.drawRect(0, 0, SCREEN_SIZE.W, SCREEN_SIZE.H);
    stage.endFill();

    PIXI.ticker.shared.start();

    this.initEvents();
    this.initScheduler();
    this.gameLoop();
    initWorker(this.renderer.context, function (pixels) {
      return shapeOccupied.innerHTML = pixels;
    });
  }

  createClass(Game, [{
    key: 'initEvents',
    value: function initEvents() {
      var _this2 = this;

      var handlers = document.querySelector('.shape-counter.handlers');
      handlers.querySelector('.minus').onclick = function () {
        return _this2.changeShapePerSecond(-1);
      };
      handlers.querySelector('.plus').onclick = function () {
        return _this2.changeShapePerSecond(1);
      };

      handlers = document.querySelector('.gravity.handlers');
      handlers.querySelector('.minus').onclick = function () {
        return _this2.changeGravity(-1);
      };
      handlers.querySelector('.plus').onclick = function () {
        return _this2.changeGravity(1);
      };
    }
  }, {
    key: 'initScheduler',
    value: function initScheduler() {
      var _this3 = this;

      var generateShape = function generateShape() {
        var x = Math.floor(Math.random() * window.innerWidth - WIDTH);
        var y = Math.floor(Math.random() * window.innerHeight - HEIGHT);
        var type = Math.floor(Math.random() * SHAPE.MAX);
        var dataToShape = { position: { x: x, y: y }, removeCallback: function removeCallback(shape) {
            return _this3.onRemoveShape(shape);
          } };
        var shape = Factory$1.createShape(type, dataToShape);
        _this3.onAddShape(shape);
      };

      setInterval(function () {
        var i = Number(shapePerSecond.innerHTML);
        for (; i > 0; i--) {
          generateShape();
        }
      }, 1000);
    }
  }, {
    key: 'gameLoop',
    value: function gameLoop() {
      this.renderer.render(stage);
      requestAnimationFrame(this.gameLoop.bind(this));
    }
  }, {
    key: 'onClick',
    value: function onClick(event) {
      var _this4 = this;

      var mousePosition = event.data.getLocalPosition(stage);
      var dataToShape = { position: mousePosition, removeCallback: function removeCallback(shape) {
          return _this4.onRemoveShape(shape);
        } };
      var type = Math.floor(Math.random() * SHAPE.MAX);
      var shape = Factory$1.createShape(type, dataToShape);
      this.onAddShape(shape);
    }
  }, {
    key: 'onAddShape',
    value: function onAddShape(shape) {
      stage.addChild(shape);
      this.changeShapeCount(1);
      shape.changeGravity(Number(gravity.innerHTML));
      shapes.push(shape);
    }
  }, {
    key: 'onRemoveShape',
    value: function onRemoveShape(shape) {
      stage.removeChild(shape);
      this.changeShapeCount(-1);
      shapes.splice(shapes.indexOf(shape), 1);
    }
  }, {
    key: 'changeShapeCount',
    value: function changeShapeCount(value) {
      shapeTotal.innerHTML = Number(shapeTotal.innerHTML) + value;
    }
  }, {
    key: 'changeShapePerSecond',
    value: function changeShapePerSecond(value) {
      shapePerSecond.innerHTML = Number(shapePerSecond.innerHTML) + value;
    }
  }, {
    key: 'changeGravity',
    value: function changeGravity(value) {
      gravity.innerHTML = Number(gravity.innerHTML) + value;
      shapes.forEach(function (shape) {
        return shape.changeGravity(gravity.innerHTML);
      });
    }
  }]);
  return Game;
}();

var Application = function Application() {
  classCallCheck(this, Application);

  this.game = new Game();
};

new Application();

}());
