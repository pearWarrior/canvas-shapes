'use strict';

var gulp = require('gulp'),
  g_rollup = require('gulp-rollup');

gulp.task('bundle', function () {
  gulp.src('./source/**/*.js')
    .pipe(g_rollup({
      "format": "iife",
      "plugins": [
        require("rollup-plugin-babel")({
          "presets": [
            ["es2015", {"modules": false}]
          ],
          "plugins": ["external-helpers"]
        })
      ],
      entry: './source/main.js'
    }))
    .pipe(gulp.dest('./dist'));
});

gulp.task('watch_bundle', () => {
  gulp.watch('./source/**/*.js', ['bundle']);
});


gulp.task('default', ['bundle']);
gulp.task('watch', ['bundle', 'watch_bundle']);